#!/usr/bin/env php
<?php
	use PhpSchool\CliMenu\CliMenu;
	use PhpSchool\CliMenu\Builder\CliMenuBuilder;
	use PhpSchool\CliMenu\Action\ExitAction;
	use PhpSchool\CliMenu\Action\GoBackAction;
	use PhpSchool\CliMenu\MenuItem\AsciiArtItem;

	if (version_compare(PHP_VERSION, "7.1.0") <= 0) {
		die("A PHP version of at least 7.1 is required.".PHP_EOL);
	}

	if(php_sapi_name() != "cli") {
		die("Please run in CLI mode.");
	}

	ini_set("display_errors", 1);
	error_reporting(E_ALL);

	require "vendor/autoload.php";

	require "inc/functions.php";
	require "inc/Registry.class.php";

	$menu = new CliMenuBuilder;

	// Install wp-cli menu
	$menu->addItem("1) Install wp-cli",  function(CliMenu $menu) {
		if(isWpCliInstalled()) {
			displayError("wp-cli is already installed. (use 'wp cli update' to update)", $menu);
			return;
		}

		$home = getenv("HOME");
		$installDir = $home."/bin";

		passthru("mkdir -p $installDir");

		if(!is_dir($installDir)) {
			displayError("Installation directory cannot be created automatically, please proceed manually.", $menu);
			return;
		}

		$wpCli = WpCliInstallDir();

		// Download and install the latest version of wp-cli.php
		$script = "
			wget -O $wpCli https://raw.github.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
			chmod +x $wpCli
		";

		passthru($script);

		// Do a very naive attempt at figuring out if an alias should be made in .bashrc
		$makeAlias = shell_exec("grep \"alias wp\" ~/.bashrc 2>/dev/null");
		if($makeAlias == NULL || substr($makeAlias, 0, 1) == "#") {
			passthru("echo 'alias wp=\"php $installDir/wp-cli.phar\"' >> $home/.bashrc");
		}

		displaySuccess("wp-cli has been installed successfully.", $menu);
	});

	// Install wordpress menu item
	$menu->addItem("2) Download WordPress core", function(CliMenu $menu) {
		$wp = getWpCli();

		if(isWpCliInstalled() === false) {
			displayError("wp-cli is not installed", $menu);
			return;
		}

		$wpInstallDir = realpath(__DIR__."/..");

		$locale = "en_GB";

		$installDir = ask("Enter WordPress install directory.", $wpInstallDir, $menu);

		exec("mkdir -p $wpInstallDir");
		exec("$wp core download --path=$installDir --locale=$locale 2>&1", $out, $exit);

		if($exit > 0) {
			displayError($out[0]." (Press enter to continue)", $menu);
		} else {
			displaySuccess(array_pop($out), $menu);
		}

		Registry::set("wordpress_installation_directory", $installDir);
	});

	$menu->addItem("3) Create wp-config.php", function(CliMenu $menu) {
		$wp = getWpCli();
		$wpInstallDir = getWpInstallDir($menu);

		$sampleConfigFile = rtrim($wpInstallDir, '/')."/wp-config-sample.php";

		if(file_exists($sampleConfigFile) == false) {
			displayError("Error: Please download WordPress before proceeding with this step.", $menu);
			return;
		}

		$db_host = '127.0.0.1';
		$db_user = '';
		$db_pass = '';
		$db_name = '';

		while(!isset($dbh)) {
			$db_host = ask("Database hostname: ", $db_host, $menu);
			$db_user = ask("Database username: ", $db_user, $menu);
			$db_pass = ask("Database password: ", $db_pass, $menu);
			$db_name = ask("Database name: ", $db_name, $menu);

			$dsn = "mysql:dbname=$db_name;host=$db_host";

			try {
				$dbh = new PDO($dsn, $db_user, $db_pass);
			} catch (PDOException $e) {
				displayError($e->getMessage(), $menu);
			}
		}

		$charset = Registry::get("config_database_charset");
		$prefix  = Registry::get("config_database_prefix");

		$configCreate = "$wp core config"
						.' --dbcharset="'.$charset.'"'
						.' --dbprefix="'.$prefix.'"'
						.' --dbhost="'.$db_host.'"'
						.' --dbuser="'.$db_user.'"'
						.' --dbpass="'.$db_pass.'"'
						.' --dbname="'.$db_name.'"'
						.' --path="'.$wpInstallDir.'"'
						." --skip-check 2>&1";

		exec($configCreate, $out2, $exit);

		if($exit > 0) {
			displayError($out2[0], $menu);
		} else {
			displaySuccess(array_pop($out2), $menu);
		}
	});

	$menu->addItem("4) Configure wordpress installation", function(CliMenu $menu) {
		$wp = getWpCli();
		$wpInstallDir = getWpInstallDir($menu);

		exec("$wp core version --path=".$wpInstallDir." 2>&1", $version, $exit);

		if($exit < 1) {
			$url = "http://".gethostname();

			while(true) {
				$url 	= ask("Site URL: ", $url, $menu);

				if($url != filter_var($url, FILTER_VALIDATE_URL)) {
					displayError("Please enter a valid url", $menu);
				} else {
					break;
				}
			}

			$title  = ask("Site title: ", null, $menu);
			$user 	= ask("Admin username: ", null, $menu);

			while(true) {
				$email 	= ask("Admin e-mail: ", null, $menu);

				if($email != filter_var($email, FILTER_VALIDATE_EMAIL)) {
					displayError("Please enter a valid e-mail", $menu);
				} else {
					break;
				}
			}

			if(Registry::get("config_generate_admin_password") > 0) {
				$pass 	= randomString();
			} else {
				while(true) {
					$pass = ask("Admin password: ", $pass, $menu);

					if(mb_strlen($pass) < 6 || !preg_match("[a-zA-Z]", $pass) || !preg_match("[0-9]", $pass)) {
						displayError("Password must be stronger than that.");
					} else {
						break;
					}
				}
			}

			$wpInstallCmd = "$wp core install"
							.' --path="'.$wpInstallDir.'"'
							.' --url="'.$url.'"'
							.' --title="'.$title.'"'
							.' --admin_user="'.$user.'"'
							.' --admin_email="'.$email.'"'
							.' --admin_password="'.$pass.'"'
							.' --skip-email 2>&1';

			exec($wpInstallCmd, $installOutput, $exit2);

			if($exit2 < 1) {
				displaySuccess("Wordpress installed successfully", $menu);
				displaySuccess("Your password is: ".$pass, $menu);
			} else {
				displayError($installOutput[1], $menu);
			}
		} else {
			displayError($version[0], $menu);
		}
	});

	$menu->addLineBreak('-');

	$menu->addSubMenu("Language settings", function(CliMenuBuilder $build) {
		$build->disableDefaultItems();

		$build->addItem("Install core language pack", function(CliMenu $menu) {
			$wp = getWpCli();
			$wpInstallDir = getWpInstallDir($menu);

			while(true) {
				$locale = ask("Enter language code to install", Registry::get("config_installation_language"), $menu);

				if(preg_match("/^[a-z]{2}\_[A-Z]{2}/", $locale)) {
					Registry::set("config_installation_language", $locale);
					break;
				}

				displayError("Please enter a valid language IETF Language tag, such as en_GB", $menu);
			}

			$languageCmd = "$wp language core install $locale --path=$wpInstallDir --activate";

			exec($languageCmd, $out, $exit);

			if($exit < 1) {
				displaySuccess("Language installed successfully", $menu);
			} else {
				displayError($out[1], $menu);
			}
		});

		$build->addItem("Remove core language pack", function(CliMenu $menu) {
			$wp = getWpCli();
			$wpInstallDir = getWpInstallDir($menu);

			$removeableLanguages = getInstalledLanguages();

			if(!empty($removeableLanguages)) {
				print "The following languages are removeable:".PHP_EOL.PHP_EOL;

				foreach($removeableLanguages as $code => $removeable) {
					print "    * $code: $removeable";
				}

				while(true) {
					$locale = ask("Enter language code to remove", key($removeableLanguages), $menu);

					if(isset($removeableLanguages[$locale])) {
						break;
					} else {
						displayError("Cannot remove '$locale' language is either active or not installed.", $menu);
					}
				}

				$languageCmd = "$wp language core uninstall $locale --path=$wpInstallDir 2>&1";

				exec($languageCmd, $out, $exit);

				if($exit < 1) {
					displaySuccess("Language successfully uninstalled", $menu);
				} else {
					displayError($out[1] ?? "Unknown error", $menu);
				}
			} else {
				displayError("No other languages installed.", $menu);
			}
		});

		$build->setTitle("Language management settings");
		$build->addLineBreak('-');
		$build->addItem('Return to main menu', new GoBackAction); //add a go back button
	});

	// Miscellaneous submenu
	$menu->addSubMenu("Miscellaneous settings", function(CliMenuBuilder $build) {
		$build->disableDefaultItems();

		$build->addItem("Database character set", function(CliMenu $submenu) {
			$charset = ask("Set table prefix", Registry::get("config_database_charset"), $submenu);
			Registry::set("config_database_charset", $charset);
		});

		$build->addItem("Database table prefix", function(CliMenu $submenu) {
			$prefix = ask("Set table prefix", Registry::get("config_database_prefix"), $submenu);
			Registry::set("config_database_prefix", $prefix);
		});

		$build->addItem("Autogenerate admin password", function(CliMenu $submenu) {
			$autogenerate = askNumber("Automically set admin user password (0=Disable, 1=Enable)", Registry::get("config_generate_admin_password"), $submenu);
			Registry::set("config_generate_admin_password", $autogenerate);
		});

		$build->setTitle("Miscellaneous installation settings");
		$build->addLineBreak('-');
		$build->addItem('Return to main menu', new GoBackAction); //add a go back button
	});

	$menu->addSubMenu("Delete this tool", function(CliMenuBuilder $build)  {
		$build->disableDefaultItems();
		$build->setTitle("Confirm deletion: ".__DIR__." ?");
		$build->addItem("Yes - Permanently delete this tool", function(CliMenu $submenu) {
			$submenu->close();
			$dir = __DIR__;

			chdir("../");
			passthru("rm -rf ".$dir);
			passthru("cd ..");

			exit(0);
		});
		$build->addItem("No - Go back to main menu", new GoBackAction); //add a go back button
	});

	$menu->addLineBreak('-');

	$menu->addSubMenu("Credits", function(CliMenuBuilder $build) {
		$credits = file_get_contents(__DIR__."/credits.txt").PHP_EOL;

		$build->disableDefaultItems();
		$build->addAsciiArt($credits, AsciiArtItem::POSITION_CENTER)->build();
		$build->addLineBreak('-');
		$build->addItem('Return to main menu', new GoBackAction); //add a go back button
	});

	$menu->setTitle("WordPress CLI Install Tool");
	$menu->setBorderLeftWidth(2);
	$menu->setBackgroundColour("52");
	$menu->setBorderColour("07");
	$menu->setPadding(2, 4);
	$menu->setMarginAuto();
	$build = $menu->build();

	$build->addCustomControlMapping("q", function(CliMenu $menu) {
		$menu->close();
	});

	$build->open();