 __          _______    _____           _        _ _   _______          _ 
 \ \        / /  __ \  |_   _|         | |      | | | |__   __|        | |
  \ \  /\  / /| |__) |   | |  _ __  ___| |_ __ _| | |    | | ___   ___ | |
   \ \/  \/ / |  ___/    | | | '_ \/ __| __/ _` | | |    | |/ _ \ / _ \| |
    \  /\  /  | |       _| |_| | | \__ \ || (_| | | |    | | (_) | (_) | |
     \/  \/   |_|      |_____|_| |_|___/\__\__,_|_|_|    |_|\___/ \___/|_|
                                                                          
                By Allan Rehhoff <allan.rehhoff@gmail.com>
                        Licensed under: WTFPL
                           Powered by: PHP

#Introduction#
WP Install Tool is a command line (CLI) tool to download and install latest version of wp-cli and wordpress.

#Installation#
1. Clone this repository
2. $ chmod +x main.php
3. $ ./main.php
4. Follow the on screen instructions

Don't forgot to delete this tool once you're done installing. This can be done through the built-in menu item.

#Security#
This tool was not written with much security in mind, so only run this in a trusted environment.  
Passwords are also exposed, so don't use this tool at a public place. 