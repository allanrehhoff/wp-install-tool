<?php
	function displayError(string $error, $menu) {
		$popupStyle = (new PhpSchool\CliMenu\MenuStyle)->setBg("red")->setFg("white");
		$menu->flash($error, $popupStyle)->display();

		writeToLog($error, "ERROR");
	}

	function displayWarning(string $message, $menu) {
		$popupStyle = (new PhpSchool\CliMenu\MenuStyle)->setBg("94")->setFg("white");
		$menu->flash($message, $popupStyle)->display();

		writeToLog($message, "WARNING");
	}

	function displaySuccess(string $message, $menu) {
		$popupStyle = (new PhpSchool\CliMenu\MenuStyle)->setBg("green")->setFg("white");
		$menu->flash($message, $popupStyle)->display();

		writeToLog($message, "SUCCESS");
	}

	function getInstalledLanguages() {
		$wp = getWpCli();

		$installedLanguages = shell_exec("$wp language core list --format=json");
		$installedLanguages = json_decode($installedLanguages);

		$languageArray = [];
		foreach($installedLanguages as $language) {
			if($language->status == "installed") {
				$languageArray[$language->language] = $language->english_name;
			}
		}

		return $languageArray;
	}

	function ask($question, $placeholder = '', $menu) {
		if(!$placeholder) {
			$placeholder = '';
		}

		return $menu->askText()
					->setPromptText($question)
					->setPlaceholderText($placeholder)
					->setValidationFailedText('Please a non-empty value')
					->ask()
					->fetch();
	}

	function askNumber($question, $placeholder = '', $menu) {
		if(!$placeholder) {
			$placeholder = '';
		}

		return $menu->askNumber()
					->setPromptText($question)
					->setPlaceholderText((int)$placeholder)
					->setValidationFailedText('Please a numeric value value')
					->ask()
					->fetch();
	}

	function getWpInstallDir($menu) {
		if(Registry::has("wordpress_installation_directory")) {
			$wpInstallDir = Registry::get("wordpress_installation_directory");
		} else {
			$wpInstallDir = ask("Enter wordpress installation directory", realpath(__DIR__."/../.."), $menu);
		}

		Registry::set("wordpress_installation_directory", $wpInstallDir);

		return $wpInstallDir;
	}

	function getWpCli() {
		if(isWpCliInstalled()) {
			return WpCliInstallDir();
		} else {
			return "wp";
		}
	}

	/**
	* Gets the pathn to the current logfile, relative to current script location
	*/
	function getLogfilePath() {
		$mainDir = realpath(__DIR__."/../");
		$logfile = ".output-".date("Y-m-d").".log";

		return $mainDir.'/'.$logfile;
	}

	/**
	* Writes a formatted string to the logfile
	*/
	function writeToLog($message, $type) {
		$type = strtoupper($type);

		return file_put_contents(getLogfilePath(), $type."\t".date("Y-m-d H:i:s")."\t".$message.PHP_EOL, FILE_APPEND);
	}

	/**
	* Recursively delete a directory
	*/
	function rrmdir($dir) { 
		if (is_dir($dir)) {
			$objects = scandir($dir);
			foreach ($objects as $object) {
				if ($object != "." && $object != "..") {
					if (is_dir($dir."/".$object)) {
						rrmdir($dir."/".$object);
					} else {
						unlink($dir."/".$object);
					}
				}
			}

			rmdir($dir); 
		}
	}

	function WpCliInstallDir() {
		return getenv("HOME")."/bin/wp-cli.phar";
	}

	/**
	* Checks if wp-cli is available to the current script
	*/
	function isWpCliInstalled() {
		return file_exists(WpCliInstallDir());
	}

	/**
	* Generates a random string, should be compatible with most servers.
	* @return string
	*/
	function randomString($length = 12) {
		if(!isset($length) || intval($length) <= 8){
			$length = 12;
		}

		if (function_exists("random_bytes")) {
			return bin2hex(random_bytes($length));
		}

		if (function_exists("mcrypt_create_iv")) {
			return bin2hex(mcrypt_create_iv($length, MCRYPT_DEV_URANDOM));
		}

		if (function_exists("openssl_random_pseudo_bytes")) {
			return bin2hex(openssl_random_pseudo_bytes($length));
		}
	}