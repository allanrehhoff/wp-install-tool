<?php
	/**
	* Registry to hold class objects and data.
	* @author Allan Rehhoff
	*/
	class Registry {
		/**
		* @var
		* Stores the data objectsm and other config values
		*/
		private static $data = [];

		/**
		* @var
		* Default configuration values.
		*/
		private static $defaults = [
			"config_installation_language" => "da_DK",
			"config_database_charset" => "utf8mb4",
			"config_database_prefix" => "wp_",
			"config_generate_admin_password" => 1
		];

		/**
		* Get an object by it's class name, namespaces included.
		* @return (object) on success, null if the object is nowhere to be found
		*/
		public static function get($key) {
			if(isset(self::$data[$key])) {
				return self::$data[$key];
			}

			return (isset(self::$defaults[$key]) ? self::$defaults[$key] : NULL);
		}

		/**
		* Stores a given object by it's class name, namespaces included
		* @return (object) The instance just stored. 
		*/
		public static function set($key, $data) {
			self::$data[$key] = $data;

			return $data;
		}

		/**
		* Checks if the registry contains a given object.
		* @return bool
		*/
		public static function has($key) {
			return isset(self::$data[$key]);
		}
	}